
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;

-- Dumping database structure for rars
DROP DATABASE IF EXISTS `rars`;
CREATE DATABASE IF NOT EXISTS `rars` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `rars`;


-- Dumping structure for table rars.tmtrace
DROP TABLE IF EXISTS `tmtrace`;
CREATE TABLE IF NOT EXISTS `tmtrace` (
	`id` BIGINT(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique trace dataset ID',
	`robot` VARCHAR(8) NOT NULL COMMENT 'Unique robot identifier',
	`tmtime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Current timestamp (auto-generated)',
	`starttime` FLOAT NULL DEFAULT NULL COMMENT 'Time when the race was started (situation\'s start_time)',
	`racetime` FLOAT NULL DEFAULT NULL COMMENT 'Elapsed time since start of race (situation\'s time_count)',
	`laps` INT(11) NULL DEFAULT NULL COMMENT 'Number of laps done within this race (situation\'s laps_done)',
	`velocity` FLOAT NULL DEFAULT NULL COMMENT 'Current velocity',
	`fuel` FLOAT(11) NULL DEFAULT NULL COMMENT 'Current fuel amount',
	`damage` INT(11) NULL DEFAULT NULL COMMENT 'Current damage count',
	`userdata` VARCHAR(256) NULL DEFAULT NULL COMMENT 'Individual user data field (optional)',
	PRIMARY KEY (`id`)
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Add user for RARS trace system
CREATE USER 'rars'@'localhost' IDENTIFIED BY 'r2a0c1e4';
GRANT SELECT, INSERT  ON `rars`.* TO 'rars'@'localhost';
FLUSH PRIVILEGES;
